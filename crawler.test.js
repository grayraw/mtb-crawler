const {grabPages, processLink, extractFirstPage, extractTheRestofthePages} = require('./server.js');
var _ = require('underscore')._;
// console.log(extract);

let giant = {
    url: "https://www.giant-bicycles.com/us/bikes/off-road",
    firstLevelFilter: '.generic-summary a[href^="/us/bikes"]', // links that start with /us/bikes in a.href
    secondLevelFilter: '[data-analytics-impression="Bike Series"]' // data-analytics-impression - 'bike series'
}

describe('check crawler on giant example', ()=>{
    beforeEach(()=>{
        grabPages(giant)
    })

    test('fetches a first page', () => {
        expect.assertions(1);
        return extractFirstPage().then((data)=>{
            expect(data[0][0]).toHaveProperty('_root');
        })
    });

    test('resolves absolute links', ()=>{
        let testLink = {attribs: {href: 'https://www.giant-bicycles.com/us/bikes-atx'}};
        expect( processLink(0, testLink) ).toBe('https://www.giant-bicycles.com/us/bikes-atx')
    })

    test('resolves root links', ()=>{
        let testLink = {attribs: {href: '/us/bikes-atx'}};
        expect( processLink(0, testLink) ).toBe('https://www.giant-bicycles.com/us/bikes-atx')
    })

    test('resolves relative links', ()=>{
        let testLink = {attribs: {href: 'us/bikes-atx'}};
        expect( processLink(0, testLink) ).toBe('https://www.giant-bicycles.com/us/bikes/off-road/us/bikes-atx')
    })

    xtest('filters set of links', () => {
        jasmine.DEFAULT_TIMEOUT_INTERVAL = 100000;
        expect.assertions(1);
        const mock = ['https://www.giant-bicycles.com/us/bikes-anthem-advanced-2018'];
        return extractFirstPage().then(()=>{
            return extractTheRestofthePages().then((data)=>{ 
                return expect(data.collectedLinks).toEqual(expect.arrayContaining(mock)) })
        })
    });

    test('filters second set of links', () => {
        jasmine.DEFAULT_TIMEOUT_INTERVAL = 100000;
        expect.assertions(1);
        const mock = ['https://www.giant-bicycles.com/us/anthem-advanced-2-2018'];
        return extractFirstPage().then(()=>{
            return extractTheRestofthePages().then(()=>{
                return extractTheRestofthePages().then((data)=>{
                    return expect(data.collectedLinks).toEqual(expect.arrayContaining(mock)) })
            })
        })
    });

})