let fs = require('fs');
let fileList = require('./companies.js')
// let fileNames = Object.keys(fileList);
let fileNames = ['giant'];
let cheerio = require('cheerio');

let counter = 0;
fileNames.forEach((name, index)=>{
    fs.readFile(`${name}.json`, 'utf8', (err, data)=> {
        counter ++
        if (err) console.log(err);
        // if (data) console.log(JSON.parse(data));
        let pagesArray = JSON.parse(data);
        pagesArray.forEach((page)=>{
            let cheerioPage = cheerio.load(page);
            // console.log(cheerioPage('title').html());

            // console.log(filters.giant.title.req);
            // console.log(filters.giant.image.req);
            console.log(cheerioPage(filters.giant.title.req).html());
            console.log(cheerioPage(filters.giant.image.req).attr('ng-src'));
        })
    });
})


let filters = {
    giant: {
        title: {
            req: 'title',
            regexp: ''
        },
        image: {
            req: '[name=mainImage]'
        }
    }
}