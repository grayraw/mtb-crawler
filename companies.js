module.exports = {
    giant: {
        url: ["https://www.giant-bicycles.com/us/bikes/off-road"],
        firstLevelFilter: '.generic-summary a[href^="/us/bikes"]', // links that start with /us/bikes in a.href
        secondLevelFilter: '[data-analytics-impression="Bike Series"]' // data-analytics-impression - 'bike series'
    },
    specialized: {
        url: ['https://www.specialized.com/us/en/new-arrivals/bikes/mountain-bikes/c/mountain?q=%3Aprice-desc%3Aarchived%3Afalse&show=All'],
        firstLevelFilter: '.product-image',
        // secondLevelFilter: '.colorway-tile__anchor-link'
    },
    trek: {
        url: ['https://www.trekbikes.com/nz/en_NZ/bikes/mountain-bikes/c/B300/?pageSize=72&q=%3Arelevance%3AfacetCategoryDisplay%3AMountain+bikes#'],
        firstLevelFilter: '.product-tile__link'
    },
    scott: {
        url: ['https://www.scott-sports.com/nz/en/products/bike-bikes-mountain'],
        firstLevelFilter: '.info a',
        secondLevelFilter: '.productMainLink'
    },
    cannondale: {
        url: ['http://www.cannondale.com/en/USA/Products/ProductCategory.aspx?nid=c4f0dee5-d6fb-489a-9624-11286eba7b94',
                'http://www.cannondale.com/en/USA/Products/ProductCategory.aspx?nid=ec39d358-0637-4436-9e68-d65316db8fb2',
                'http://www.cannondale.com/en/USA/Products/ProductCategory.aspx?nid=480c9613-1c53-4432-9b58-e515215f1501',
                'http://www.cannondale.com/en/USA/Products/ProductCategory.aspx?nid=919ae01a-f75e-464a-8e70-c5f4730be7f3',
                'http://www.cannondale.com/en/USA/Products/ProductCategory.aspx?nid=af33c7a2-16b2-4045-a4c3-904acf0b6b40',
                'http://www.cannondale.com/en/USA/Products/ProductCategory.aspx?nid=5bcdf9e1-acbc-4254-8c5f-0815ddcc0144'],
        firstLevelFilter: '.viewProducts'
    },

    gt: {
        url: ['http://www.gtbicycles.com/int_en/2018/bikes/mountain-fullsuspension',
        'http://www.gtbicycles.com/int_en/2018/bikes/mountain-hardtail'],
        firstLevelFilter: '.link-see-bike'
    },
    merida: {
        url: ['https://www.merida-bikes.com/en_int/bikes/full-suspension/2018/1177-bikefinder.html',
        'https://www.merida-bikes.com/en_int/bikes/hardtails/2018/1178-bikefinder.html', 
        'https://www.merida-bikes.com/en_int/bike/finder?year=2018&sortDirection=asc&categories%5B%5D=1179&applications%5B%5D=1191&weight=&price='],
        firstLevelFilter: '#search-results .images a'
    },
    fuji: {
        url: [''],
        firstLevelFilter: ''
    },
    santaCruz: {
        url: [''],
        firstLevelFilter: ''
    },
    kona: {
        url: [''],
        firstLevelFilter: ''
    }

}


/*
giant
specialized
trek
scott
cannondale
gt
merida
fuji
santa cruz
kona
*/

/*
yeti
diamondback
marin
jamis
rocky mountain

*/