// refactor this eh?

var express = require('express');
var fs = require('fs');
var request = require('request');
var cheerio = require('cheerio');
var fetch = require('node-fetch');
var app     = express();
var _ = require('underscore')._;
var deferred = require('deferred');
var promisify = require('deferred').promisify;
const puppeteer = require('puppeteer');

(async () => {
  const browser = await puppeteer.launch();
  const page = await browser.newPage();
  await page.goto('https://www.giant-bicycles.com/us/anthem-advanced-0');
  await page.screenshot({path: 'example.png'});

  await browser.close();
})();

var companies = require('./companies.js');

//vars need to be global for extract to have access;
let filters, pages, step, totalSteps, company, companyName;

function grabPages(companyObject, compName){
    company = companyObject;
    companyName = compName || '';
    filters = _.values(companyObject).slice(1);

    pages = [];
    step = 0;
    totalSteps = filters.length;

    extractPages(company.url)
}


function extractPages(collectedLinks){

    let counter = 0;
    pages[step] = [];
    console.log(collectedLinks[0]);
    console.log(collectedLinks[1]);
    console.log(collectedLinks[2]);
    collectedLinks.forEach((link)=>{

        fetch(link).then((response)=>{
            response.text().then((data)=>{
                pages[step].push(cheerio.load(data));
                counter = counter + 1;
                if(counter === collectedLinks.length) {
                    console.log(`Step: ${step}`);
                    console.log(`Pages collected for ${companyName}: ${pages[step].length}`)
                    if (step < totalSteps) {
                        let newSetOfLinks = findMatchingLinks(pages[step], filters[step]);
                        step = step + 1;
                        extractPages(newSetOfLinks)
                    }
                    else if (step === totalSteps) dumpResult(pages, collectedLinks)
                }
            })
        })
        
    })
}

function dumpResult(pages, collectedLinks){
        //take the last set of pages
        let arrayToWrite = [];
        let linksToWrite = [];
        pages[pages.length - 1].forEach( (page)=>{
            arrayToWrite.push(page.html());
        })
        fs.writeFile(`${companyName}.json`, JSON.stringify(arrayToWrite, null, 4), function(err){
            console.log('all done')
        })
        // console.log(collectedLinks);
        collectedLinks.forEach( (link)=>{
            linksToWrite.push(link);
        })
        fs.writeFile(`${companyName}-links.json`, JSON.stringify(linksToWrite, null, 4), function(err){
            console.log('all done')
        })
}

function findMatchingLinks(pagesToCycle, linkFilter){
    console.log(linkFilter);
    let linksArray = []
    //cycle all previously collected pages
    pagesToCycle.forEach(($page)=>{
        //grab links by filter
        //page is a cheerio object
        $page(linkFilter).each((index, element)=>{
            let processedLink = processLink(index, element);
            linksArray.push(processedLink)
        });
    })
    return linksArray;
}

//checks what kind of link is found and returns it changed
function processLink (index, element){
    let exctractedLink = element.attribs['href'];
    if (exctractedLink.substring(0,4) === 'http'){
        //this is absolute link
        return exctractedLink
    } else if (exctractedLink.substring(0,1) === '/') {
        //this is root link
        let root = company.url[0].match(/.*\/\/.[^\/]*/g)
        return root + exctractedLink;
    } else if (exctractedLink === 'javascript:void(0)'){
        //cannondale bullshit
        // console.log(element.attribs.productid)
        return `http://www.cannondale.com/en/USA/Bike/ProductDetail?Id=${element.attribs.productid}`
    } else {
        //this is a relative link  //will work only with first level filter
        //wont work with array
        return company.url[0] + '/' + exctractedLink;
    }
}

// grabPages(giant);
// grabPages(companies.giant, 'giant');
// grabPages(companies.specialized, 'specialized');
// grabPages(companies.trek, 'trek');
// grabPages(companies.scott, 'scott');
// grabPages(companies.cannondale, 'cannondale');


app.listen('8081')
console.log('Magic happens on port 8081');
module.exports = app;
module.exports.grabPages = grabPages;
module.exports.processLink = processLink;
// module.exports.extractFirstPage = extractFirstPage;
module.exports.extractPages = extractPages;
